﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour 
{
	public Tilemap tilemapRocks;
	public Tile dirtTile;

	public static GameManager Instance { get; private set; }

	void Awake()
	{
		if(Instance == null)
			Instance = this;
		else if(Instance != this)
			Destroy(gameObject);
	}

	public void BreakRock(Vector3Int pos)
	{
		Tile tile = tilemapRocks.GetTile(pos) as Tile;

		if(tile != null)
			tilemapRocks.SetTile(pos, dirtTile);
	}
}
