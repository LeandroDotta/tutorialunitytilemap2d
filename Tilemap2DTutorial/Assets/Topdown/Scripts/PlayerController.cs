﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Tilemap2DTutorial.Topdown
{
	public enum MoveDirection
	{
		Up = 0,
		Right = 1,
		Down = 2,
		Left = 3
	}

	public class PlayerController : MonoBehaviour 
	{
		public float speed;
		public Grid grid;
		private Tilemap[] tilemaps;

		private MoveDirection currentDirection = MoveDirection.Down;
		private Vector3Int currentPosition;

		private Animator anim;

		private bool moving;
		private bool attacking;

		public bool IsBusy { get{ return moving || attacking; } }

		void Start()
		{
			currentPosition = grid.WorldToCell(transform.position);
			Vector3 cellCenter = grid.GetCellCenterWorld(currentPosition);
			transform.position = cellCenter;

			tilemaps = grid.GetComponentsInChildren<Tilemap>();
			anim = GetComponent<Animator>();
		}

		void Update()
		{
			float axisHorizontal = Input.GetAxisRaw("Horizontal");
			float axisVertical = Input.GetAxisRaw("Vertical");

			if(axisHorizontal != 0)
				Move(axisHorizontal == 1 ? MoveDirection.Right : MoveDirection.Left);

			if(axisVertical != 0)
				Move(axisVertical == 1 ? MoveDirection.Up : MoveDirection.Down);

			if(Input.GetButtonDown("Attack"))
				AttackStart();

			anim.SetBool("walking", moving);
		}

		private void Attack()
		{
			Vector3Int pos = GetNextPosition(currentDirection);
			GameManager.Instance.BreakRock(pos);
		}

		private void AttackStart()
		{
			if(!IsBusy)
			{
				attacking = true;
				anim.SetTrigger("attack");
			}
		}

		private void AttackEnd()
		{
			attacking = false;
		}

		private void Move(MoveDirection direction)
		{

			currentDirection = direction;
			anim.SetInteger("direction", (int)direction);

			if(!IsBusy)
			{
				anim.SetTrigger("turn");
				Vector3Int nextPosition = GetNextPosition(direction);

				if(IsWalkable(nextPosition))
				{
					currentPosition = nextPosition;
					Vector3 cellCenter = grid.GetCellCenterWorld(nextPosition);

					StartCoroutine(MoveCoroutine(transform.position, cellCenter));
				}
			}
		}

		private IEnumerator MoveCoroutine(Vector2 from, Vector2 to)
		{
			moving = true;
			Vector2 direction = (to-from).normalized;

			while(true)
			{
				Vector2 pos = transform.position;
				pos += direction * speed * Time.deltaTime;

				float minX = Mathf.Min(from.x, to.x);
				float maxX = Mathf.Max(from.x, to.x);
				float minY = Mathf.Min(from.y, to.y);
				float maxY = Mathf.Max(from.y, to.y);

				pos.x = Mathf.Clamp(pos.x, minX, maxX);
				pos.y = Mathf.Clamp(pos.y, minY, maxY);

				transform.position = pos;

				if((Vector2)transform.position == to)
					break;

				yield return new WaitForEndOfFrame();
			}

			moving = false;
		}

		private bool IsWalkable(Vector3Int tilePos)
		{
			for(int i = 0; i < tilemaps.Length; i++)
			{
				Tile tile = tilemaps[i].GetTile(tilePos) as Tile;

				if(tile != null && tile.colliderType != Tile.ColliderType.None)
					return false;
			}

			return true;
		}

		private Vector3Int GetNextPosition(MoveDirection direction)
		{
			Vector3Int pos = currentPosition;

			switch (direction)
			{
				case MoveDirection.Up:
					pos = currentPosition + Vector3Int.up;
					break;

				case MoveDirection.Down:
					pos = currentPosition + Vector3Int.down;
					break;

				case MoveDirection.Left:
					pos = currentPosition + Vector3Int.left;
					break;

				case MoveDirection.Right:
					pos = currentPosition + Vector3Int.right;
					break;
			}

			return pos;
		}
	}
}

