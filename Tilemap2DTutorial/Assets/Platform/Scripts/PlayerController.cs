﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tilemap2DTutorial.Platformer
{
	public class PlayerController : MonoBehaviour 
	{
		public float speed;
		public float jumpForce;
		public LayerMask groundCheckLayer;

		private bool jump;
		private bool grounded;
		private float axisHorizontal;
		private Rigidbody2D rb2d;
		private Collider2D coll;
		private Animator anim;

		void Start()
		{
			rb2d = GetComponent<Rigidbody2D>();
			coll = GetComponent<Collider2D>();
			anim = GetComponent<Animator>();
		}

		void FixedUpdate()
		{
			rb2d.velocity = new Vector2(axisHorizontal * speed, rb2d.velocity.y);
			anim.SetFloat("velocityX", Mathf.Abs(rb2d.velocity.x));

			if(jump && grounded)
			{
				jump = false;
				rb2d.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);

				anim.SetTrigger("jump");
			}

			RaycastHit2D hitGround = Physics2D.BoxCast(new Vector2(coll.bounds.center.x, coll.bounds.min.y), new Vector2(0.7f, 0.05f), 0, Vector2.zero, 0, groundCheckLayer);
			grounded = hitGround;
			anim.SetBool("grounded", grounded);
		}

		void Update()
		{
			axisHorizontal = Input.GetAxisRaw("Horizontal");
			Flip(axisHorizontal);
			
			if(grounded)
				jump = Input.GetButtonDown("Jump");
		}

		void OnDrawGizmos()
		{
			Gizmos.color = Color.red;

			coll = GetComponent<Collider2D>();
			Gizmos.DrawWireCube(new Vector2(coll.bounds.center.x, coll.bounds.min.y), new Vector2(0.7f, 0.05f));
		}

		private void Flip(float direction)
		{
			Vector3 scale = transform.localScale;

			if(direction > 0)
				scale.x = 1;
			else if(direction < 0)
				scale.x = -1;

			transform.localScale = scale;
		}
	}
}